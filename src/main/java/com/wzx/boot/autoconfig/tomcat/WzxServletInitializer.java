package com.wzx.boot.autoconfig.tomcat;

import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

public class WzxServletInitializer implements ServletContainerInitializer {

    private DispatcherServlet servlet;

    public WzxServletInitializer(DispatcherServlet servlet) {
        this.servlet = servlet;
    }

    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        ServletRegistration.Dynamic registration = ctx.addServlet("app", servlet);
        registration.addMapping("/");
    }
}
