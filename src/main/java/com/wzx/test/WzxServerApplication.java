package com.wzx.test;

import com.wzx.boot.annotation.SpringBootApplication;
import com.wzx.boot.autoconfig.tomcat.EnableTomcatAutoConfiguration;
import com.wzx.boot.autoconfig.web.EnableSpringWebAutoConfiguration;
import com.wzx.boot.core.ServerApplication;

@EnableSpringWebAutoConfiguration
@EnableTomcatAutoConfiguration
@SpringBootApplication
public class WzxServerApplication {
    public static void main(String[] args) {
        // 他是什么时候启动的tomcat,
        // 又是什么时候去装载的 javax.servlet.ServletContainerInitializer 文件
        // 并没有加载 ServletContainerInitializer 文件初始化，
        // 通过上下文中的 addServletContainerInitializer 初始化， 绑定DispatcherServlet
        ServerApplication.run(WzxServerApplication.class, args);
    }
}
