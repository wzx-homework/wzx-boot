package com.wzx.test.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("app")
public class UserController {
    @GetMapping("test")
    public String hello() {
        return "hello";
    }
}
