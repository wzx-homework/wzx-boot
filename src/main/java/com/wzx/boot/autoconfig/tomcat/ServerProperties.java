package com.wzx.boot.autoconfig.tomcat;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:/server.properties")
public class ServerProperties {
    private int port;
    private String path;

    public ServerProperties(Environment env) {
        this.port = Integer.parseInt(env.getProperty("port", "8080"));
        this.path = env.getProperty("path", "/");
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
